import cache.LFUCache;
import cache.LRUCache;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 28.07.2017.
 */
public final class Runner {
    private Runner() {
    }

    /**
     *
     * @param args String[]
     */
    public static void main(final String[] args) {
        Map<Integer, String> lruCache = new LRUCache<Integer, String>(5, 5);
        Map<Integer, String> lfuCache = new LFUCache<Integer, String>(5, 0.20f);
        Map<Integer, String> map = new HashMap<>(5);
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        lruCache.putAll(map);
        lfuCache.putAll(map);
        lruCache.get(1); lfuCache.get(1);
        lruCache.get(2); lfuCache.get(1);
        lruCache.get(3); lfuCache.get(3);
        lruCache.get(5); lfuCache.get(4);
        lfuCache.get(5);
        lfuCache.get(2);
        lruCache.put(6, "six");
        lfuCache.put(6, "six");
        System.out.println("LRU cache: " + lruCache.toString());
        System.out.println("LFU cache: " + lfuCache.toString());
    }

}
