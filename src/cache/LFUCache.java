package cache;

import java.util.*;

/**
 * Created by Admin on 25.07.2017.
 * @param <K> Key
 * @param <V> value
 */
public class LFUCache<K, V> extends HashMap<K, V> {
    private final Map<K, CacheNode<K, V>> cache;
    private final LinkedHashSet[] frequencyList;
    private int lowestFrequency;
    private int maxFrequency;
    private final int maxCacheSize;
    private final float evictionFactor;

    private static final float DEFAULT_EVICTION_FACTOR = 0.75f;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;

    /**
     *
     * @param capasity int
     * @param evictionFactor float
     */
    public LFUCache(final int capasity, final float evictionFactor) {
        this.cache = new HashMap<K, CacheNode<K, V>>(capasity);
        this.frequencyList = new LinkedHashSet[capasity * 2];
        this.lowestFrequency = 0;
        this.maxFrequency = capasity * 2 - 1;
        this.maxCacheSize = capasity;
        this.evictionFactor = evictionFactor;
        initFrequencyList();
    }
    /**
     *
     * @param capasity integer
     */
    public LFUCache(final int capasity) {
       this(capasity, DEFAULT_EVICTION_FACTOR);
    }

    /**
     * Default constructor
     */
    public LFUCache() {
        this(DEFAULT_INITIAL_CAPACITY, DEFAULT_EVICTION_FACTOR);
    }



    /**
     *
     * @param key tyre <K>
     * @param value type <V>
     * @return old value <V>
     */
    @Override
    public synchronized V put(final K key, final V value) {
        V oldValue = null;
        CacheNode<K, V> currentNode = cache.get(key);
        if (currentNode == null) {
            if (cache.size() == maxCacheSize) {
                doEviction();
            }
            LinkedHashSet<CacheNode<K, V>> nodes = frequencyList[0];
            currentNode = new CacheNode(key, value, 0);
            nodes.add(currentNode);
            cache.put(key, currentNode);
            lowestFrequency = 0;
         } else {
            oldValue = currentNode.v;
            currentNode.v = value;
        }
        return oldValue;
    }

    /**
     *
     * @param map Map<? extends K,? extends V> type
     */
    @Override
    public void putAll(final Map<? extends K, ? extends V> map) {
        for (Map.Entry<? extends K, ? extends V> me : map.entrySet()) {
            put(me.getKey(), me.getValue());
        }
    }

    /**
     *
     * @param key Object
     * @return type V value of CacheNode
     */
    public synchronized V get(final Object key) {
        CacheNode<K, V> currentNode = cache.get(key);
        if (currentNode != null) {
            int currentFrequency = currentNode.frequency;
            if (currentFrequency < maxFrequency) {
                int nextFrequency = currentFrequency + 1;
                LinkedHashSet<CacheNode<K, V>> currentNodes = frequencyList[currentFrequency];
                LinkedHashSet<CacheNode<K, V>> newNodes = frequencyList[nextFrequency];
                moveToNextFrequency(currentNode, nextFrequency, currentNodes, newNodes);
                cache.put((K) key, currentNode);
                if (lowestFrequency == currentFrequency && currentNodes.isEmpty()) {
                    lowestFrequency = nextFrequency;
                }
            } else {
                LinkedHashSet<CacheNode<K, V>> nodes = frequencyList[currentFrequency];
                nodes.remove(currentNode);
                nodes.add(currentNode);
            }
            return currentNode.v;
        } else {
            return null;
        }
    }

    /**
     *
     * @param key type Object
     * @return <V> type
     */
    public synchronized V remove(final Object key) {
        CacheNode<K, V> currentNode = cache.remove(key);
        if (currentNode != null) {
            LinkedHashSet<CacheNode<K, V>> nodes = frequencyList[currentNode.frequency];
            nodes.remove(currentNode);
            if (lowestFrequency == currentNode.frequency) {
                findNextLowestFrequency();
            }
            return currentNode.v;
        } else {
            return null;
        }
    }

    /**
     *
     * @param key <K> type
     * @return Integer
     */
    public synchronized int frequencyOf(final K key) {
        CacheNode<K, V> node = cache.get(key);
        if (node != null) {
            return node.frequency + 1;
        } else {
            return 0;
        }
    }

    /**
     * Clear all entries of cache.
     */
    public synchronized void clear() {
        for (int i = 0; i <= maxFrequency; i++) {
            frequencyList[i].clear();
        }
        cache.clear();
        lowestFrequency = 0;
    }

    private void initFrequencyList() {
        for (int i = 0; i <= maxFrequency; i++) {
            frequencyList[i] = new LinkedHashSet<CacheNode<K, V>>();
        }
    }

    private void findNextLowestFrequency() {
        while (lowestFrequency <= maxFrequency && frequencyList[lowestFrequency].isEmpty()) {
            lowestFrequency++;
        }
        if (lowestFrequency > maxFrequency) {
            lowestFrequency = 0;
        }
    }

    private void moveToNextFrequency(final CacheNode<K, V> currentNode,
                                     final int nextFrequency,
                                     final LinkedHashSet<CacheNode<K, V>> currentNodes,
                                     final LinkedHashSet<CacheNode<K, V>> newNodes) {
        currentNodes.remove(currentNode);
        newNodes.add(currentNode);
        currentNode.frequency = nextFrequency;
    }


    private void doEviction() {
        int currentlyDeleted = 0;
        float target = maxCacheSize * evictionFactor;
        while (currentlyDeleted < target) {
            LinkedHashSet<CacheNode<K, V>> nodes = frequencyList[lowestFrequency];
            if (nodes.isEmpty()) {
                throw new IllegalStateException("Lowest frequency constraint violated!");
            } else {
                Iterator<CacheNode<K, V>> it = nodes.iterator();
                while (it.hasNext() && currentlyDeleted++ < target) {
                    CacheNode<K, V> node = it.next();
                    it.remove();
                    cache.remove(node.k);
                }
                if (!it.hasNext()) {
                    findNextLowestFrequency();
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("{");
        String prefix = "";
        for (Map.Entry<K, CacheNode<K, V>> me : cache.entrySet()) {
            result.append(prefix).append(me.getKey()).append("=").append(me.getValue().v);
            prefix = ", ";
        }
        result.append("}");
        return result.toString();
    }

    @Override
    public int size() {
        return this.cache.size();
    }

    private class CacheNode<K, V> {
        private final K k;
        private V v;
        private int frequency;

        /**
         * @param k K type
         * @param v V type
         * @param frequency integer
         */
        CacheNode(final K k, final V v, final int frequency) {
            this.k = k;
            this.v = v;
            this.frequency = frequency;
        }
    }
}
