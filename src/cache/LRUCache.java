package cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Admin on 25.07.2017.
 * @param <K> Key
 * @param <V> Value
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {
    private final int maxEntries;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * @param initialCapacity Initial capacity
     * @param loadFactor Load factor
     * @param maxEntries Cache size
     */
    public LRUCache(final int initialCapacity,
                    final float loadFactor,
                    final int maxEntries) {
        super(initialCapacity, loadFactor, true);
        this.maxEntries = maxEntries;
    }

    /**
     * @param initialCapacity Initial capacity
     * @param maxEntries Cache size
     */
    public LRUCache(final int initialCapacity,
                    final int maxEntries) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR, maxEntries);
    }

    /**
     * @param maxEntries Cache size
     */
    public LRUCache(final int maxEntries) {
        this(DEFAULT_INITIAL_CAPACITY, maxEntries);
    }

    protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
        return size() > maxEntries;
    }

    @Override
    public synchronized V put(final K key, final V value) {
        return super.put(key, value);
    }

    @Override
    public synchronized V get(final Object key) {
        return super.get(key);
    }

    @Override
    public synchronized V remove(final Object key) {
        return super.remove(key);
    }

}
